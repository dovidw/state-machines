import { FunctionComponent } from "react";
import { CartStore } from "../services/CartStore";
import { IProduct } from "../utils/products";
import { useIsInCart } from "../utils/useIsInCart";
import { useService } from "../services/ServiceManager";

interface IProductListItemProps {
  product: IProduct;
}

export const ProductListItem: FunctionComponent<IProductListItemProps> = ({
  product,
}) => {
  const isInCart = useIsInCart(product.sku);
  const cartStore = useService(CartStore);
  return (
    <div className="product-list-item">
      <div className="product-list-item-inner">
        <h2>{product.name}</h2>
        <div className="product-list-item-image">
          <img src={product.image} alt={product.name} />
        </div>
        <div>
          {isInCart ? (
            <button>In Cart</button>
          ) : (
            <button onClick={() => cartStore.add(product.sku)}>
              Add To Cart
            </button>
          )}
        </div>
      </div>
    </div>
  );
};
