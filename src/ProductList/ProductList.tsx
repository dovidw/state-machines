import { FunctionComponent } from "react";
import { products } from "../utils/products";
import { ProductListItem } from "./ProductListItem";
import "./productList.scss";

export const ProductList: FunctionComponent = () => {
  return (
    <div className="product-list">
      {products.map((product) => (
        <ProductListItem product={product} key={product.sku} />
      ))}
    </div>
  );
};
