import { products } from "../utils/products";
import { ServiceState } from "./ServiceState";
import { ServiceManager } from "./ServiceManager";

interface ICartProduct {
  sku: number;
  price: number;
  quantity: number;
  title: string;
  image: string;
}

export interface ICartStoreState {
  items: Array<ICartProduct>;
}

export class CartStore extends ServiceState<ICartStoreState> {
  constructor(private _serviceManager: ServiceManager) {
    super({ items: [] });
  }
  add(sku: number, quantity = 1) {
    const product = products.find((product) => product.sku === sku);
    if (!product) {
      throw new Error(`Product with sku# ${sku} not found.`);
    }
    const [cartItems, existingItem] = spliceOnProductWithSku(
      this.currentState.items,
      sku
    );
    this.setState({
      items: [
        existingItem
          ? { ...existingItem, quantity: existingItem.quantity + quantity }
          : {
              sku,
              quantity,
              price: product.price,
              image: product.image,
              title: product.name,
            },
        ...cartItems,
      ],
    });
  }
  remove(sku: number) {
    const [items] = spliceOnProductWithSku(this.currentState.items, sku);
    this.setState({ items });
  }
}

function spliceOnProductWithSku(
  _products: Array<ICartProduct>,
  sku: number
): [Array<ICartProduct>, ICartProduct] | [Array<ICartProduct>] {
  const products = [..._products];

  const foundIndex = products.findIndex((product) => product.sku === sku);

  if (foundIndex < 0) {
    return [products];
  }

  const [foundProduct] = products.splice(foundIndex, 1);

  return [products, foundProduct];
}
