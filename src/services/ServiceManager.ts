import { createContext, useContext } from "react";

export type ServiceConstructorType<T> = new (
  _storeService: ServiceManager
) => T;

export class ServiceManager {
  private _services = new Map<ServiceConstructorType<any>, any>();

  getService<T>(Service: ServiceConstructorType<T>): T {
    if (this._services.has(Service)) {
      return this._services.get(Service);
    }
    const newService = new Service(this);
    this._services.set(Service, newService);
    return newService;
  }
}

export const ServiceManagerContext = createContext<ServiceManager>(
  new ServiceManager()
);

export function useService<T>(Service: ServiceConstructorType<T>): T {
  return useContext(ServiceManagerContext).getService(Service);
}
