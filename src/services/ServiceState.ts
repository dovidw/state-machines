import { useState, useEffect } from "react";
import { ServiceConstructorType, useService } from "./ServiceManager";

type TstateSubscriptionCB<S> = (state: S) => void;

export abstract class ServiceState<S> {
  constructor(private _state: S) {}
  private _subscriptions = new Set<TstateSubscriptionCB<S>>();

  setState(newState: Partial<S>) {
    this._state = { ...this._state, ...newState };
    this._subscriptions.forEach((subscription) => subscription(this._state));
  }

  subscribe(cb: TstateSubscriptionCB<S>): () => void {
    this._subscriptions.add(cb);
    return () => this._subscriptions.delete(cb);
  }

  get currentState() {
    return this._state;
  }
}

export function useServiceState<T extends ServiceState<any>>(
  Store: ServiceConstructorType<T>
): [T["currentState"], T] {
  const store = useService(Store);
  const [state, setState] = useState(store.currentState);
  useEffect(() => store.subscribe((newState) => setState(newState)), []);
  return [state, store];
}
