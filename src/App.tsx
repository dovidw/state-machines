import React from "react";
import logo from "./logo.svg";
import "./App.scss";
import { ProductList } from "./ProductList/ProductList";
import { MiniCart } from "./MiniCart/MiniCart";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <MiniCart />
      </header>
      <div>
        <ProductList />
      </div>
    </div>
  );
}

export default App;
