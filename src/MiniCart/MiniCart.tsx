import { FunctionComponent, useMemo } from "react";
import { CartStore } from "../services/CartStore";
import { useServiceState } from "../services/ServiceState";

import "./MiniCart.scss";

export const MiniCart: FunctionComponent = () => {
  const [{ items }, cartStore] = useServiceState(CartStore);

  const count = useMemo(
    () => items.reduce((count, { quantity }) => count + quantity, 0),
    [items]
  );

  return (
    <div className="mini-cart">
      <h3>{count} items in cart</h3>
      {items.length > 0 && (
        <ul className="mini-cart-list">
          {items.map(({ sku, image }) => (
            <li key={sku}>
              <img src={image} className="mini-cart-image" />
              <button onClick={() => cartStore.remove(sku)}>Remove</button>
            </li>
          ))}
        </ul>
      )}
    </div>
  );
};
