import { useMemo } from "react";
import { CartStore } from "../services/CartStore";
import { useServiceState } from "../services/ServiceState";

export function useIsInCart(sku: number) {
  const [{ items }] = useServiceState(CartStore);
  return useMemo(() => items.some((item) => item.sku === sku), [sku, items]);
}
